<?php

/**
 * @class OiiTextImageSwapModule
 */
class OiiTextImageSwapModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          	=> __('Text/Imag Swap', 'oii-bb-addons'),
			'description'   	=> __('Display Swaps Image for Text on MouseOver.', 'oii-bb-addons'),
			'category'      	=> OII_BB_ADDONS_CAT,
			'dir'             => OII_BB_ADDONS_DIR . 'modules/oii-text-image-swap/',
			'url'             => OII_BB_ADDONS_URL . 'modules/oii-text-image-swap/',
			'partial_refresh'	=> true
		));
	}

}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('OiiTextImageSwapModule', array(
	'general'       => array( // Tab
		'title'         => __('General', 'oii-bb-addons'), // Tab title
		'sections'      => array( // Tab Sections
			'general'       => array( // Section
				'title'         => '', // Section Title
				'fields'        => array( // Section Fields
					'photo'         => array(
						'type'          => 'photo',
						'label'         => __('Photo', 'oii-bb-addons')
					)
				)
			),
			'hover_text'       => array(
				'title'         => __('Text', 'oii-bb-addons'),
				'fields'        => array(
					'heading'        => array(
                        'type'            => 'text',
                        'label'           => __('Heading', 'oii-bb-addons'),
                        'default'         => '',
                        'preview'         => array(
                            'type'            => 'text',
                            'selector'        => '.fl-heading-text'
                        )
                    ),
					'hover_text'  => array(
						'type'            => 'textarea',
                        'label'           => __('Hover Text', 'oii-bb-addons'),
                        'default'         => 'Lorem ipsum dolor sit amet, has autem laboramus cu, cu justo dicunt dignissim vis. Sed in omnesque officiis qualisque. Omnium persequeris nam ex, no vis salutatus principes.',
                        'rows'          => '6',
                        'preview'         => array(
                            'type'            => 'textarea',
                            'selector'        => '.fl-hover-text'
                        )
					)
				)
			)
		)
	)
));