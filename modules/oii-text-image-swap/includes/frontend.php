<?php
?>

<div class="view view-tenth">
    <img src="<?php echo $settings->photo_src; ?>">
    <div class="mask">
        <h2><?php echo $settings->heading; ?></h2>
        <p><?php echo $settings->hover_text; ?></p>
    </div>
</div>