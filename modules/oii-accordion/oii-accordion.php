<?php

/**
 * @class OiiAccordionModule
 */
class OiiAccordionModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          	=> __('Accordion', 'oii-bb-addons'),
			'description'   	=> __('Display a collapsible accordion of items.', 'oii-bb-addons'),
			'category'      	=> OII_BB_ADDONS_CAT,
			'dir'             => OII_BB_ADDONS_DIR . 'modules/oii-accordion/',
			'url'             => OII_BB_ADDONS_URL . 'modules/oii-accordion/',
			'partial_refresh'	=> true
		));

		$this->add_css('font-awesome');
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('OiiAccordionModule', array(
	'general'       => array(
		'title'         => __('General', 'oii-bb-addons'),
		'sections'      => array(
			'content'       => array(
				'title'         => __('Content', 'oii-bb-addons'),
				'fields'        => array(
					'content'       => array(
						'type'          => 'editor',
					    'rows'          => 10,
						'wpautop'		=> false
					)
				)
			)
		)
	)
));

