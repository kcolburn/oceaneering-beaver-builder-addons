<?php global $wp_embed; ?>
<div class="fl-accordion">
	<div class="fl-accordion-item"<?php if ( ! empty( $settings->id ) ) echo ' id="' . sanitize_html_class( $settings->id ) . '-' . $i . '"'; ?>>
		<div class="fl-accordion-button">
			<span class="oii-icon-oii-expand"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>
		</div> 
		<div class="fl-accordion-content fl-clearfix"><?php echo wpautop( $wp_embed->autoembed( $settings->content ) );; ?></div>
	</div>
</div>