<?php

/**
 * This file should be used to render each module instance.
 * You have access to two variables in this file: 
 * 
 * $module An instance of your module class.
 * $settings The module's settings.
 *
 * Example: 
 */

?>
<<?php echo $settings->tag; ?> class="fl-heading">
	<span class="fl-heading-text"><?php echo $settings->heading; ?></span>
</<?php echo $settings->tag; ?>>
<?php if(!empty($settings->link)) : ?>
	<a class="see-all-link pull-right" href="<?php echo $settings->link; ?>" target="<?php echo $settings->link_target; ?>">SEE ALL  <i class="fa fa-angle-double-right"></i></a>
<?php endif; ?>