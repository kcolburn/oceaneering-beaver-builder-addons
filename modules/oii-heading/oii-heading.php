<?php

/**
 * @class oiiHeadingModule
 */
class OiiHeadingModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          	=> __('Heading', 'oii-bb-addons'),
			'description'   	=> __('Display a title/page heading.', 'oii-bb-addons'),
			'category'      	=> OII_BB_ADDONS_CAT,
			'dir'             => OII_BB_ADDONS_DIR . 'modules/oii-heading/',
			'url'             => OII_BB_ADDONS_URL . 'modules/oii-heading/',
			'partial_refresh'	=> true
		));
	}
}
$general_tab = array(
        'general'       => array(
        'title'         => __('General', 'oii-bb-addons'),
        'sections'      => array(
            'general'       => array(
                'title'         => '',
                'fields'        => array(
                    'heading'        => array(
                        'type'            => 'text',
                        'label'           => __('Heading', 'oii-bb-addons'),
                        'default'         => '',
                        'preview'         => array(
                            'type'            => 'text',
                            'selector'        => '.fl-heading-text'
                        )
                    ),
                    'heading_style'   => array(
                        'type'          => 'select',
                        'label'         => __('Heading Style', 'oii-bb-addons'),
                        'default'       => '',
                        'options'       => array(
                            ''        => __('Default Blue (for light backgrounds)', 'oii-bb-addons'),
                            'oii-heading-white'        => __('White (for dark backgrounds)', 'oii-bb-addons')
                        )
                    )
                )
            ),
            'link'          => array(
                'title'         => __('Link', 'oii-bb-addons'),
                'fields'        => array(
                    'link'          => array(
                        'type'          => 'link',
                        'label'         => __('Link', 'oii-bb-addons'),
                        'preview'         => array(
                            'type'            => 'none'
                        )
                    ),
                    'link_target'   => array(
                        'type'          => 'select',
                        'label'         => __('Link Target', 'oii-bb-addons'),
                        'default'       => '_self',
                        'options'       => array(
                            '_self'         => __('Same Window', 'oii-bb-addons'),
                            '_blank'        => __('New Window', 'oii-bb-addons')
                        ),
                        'preview'         => array(
                            'type'            => 'none'
                        )
                    )
                )
            )
        )
    ),
    'style'         => array(
            'title'         => __('Style', 'oii-bb-addons'),
            'sections'      => array(
                'colors'        => array(
                    'title'         => __('Colors', 'oii-bb-addons'),
                    'fields'        => array(
                        'color'          => array(
                            'type'          => 'color',
                            'show_reset'    => true,
                            'label'         => __('Text Color', 'oii-bb-addons')
                        ),
                    )
                ),
                'structure'     => array(
                    'title'         => __('Structure', 'oii-bb-addons'),
                    'fields'        => array(
                        'alignment'     => array(
                            'type'          => 'select',
                            'label'         => __('Alignment', 'oii-bb-addons'),
                            'default'       => 'left',
                            'options'       => array(
                                'left'      =>  __('Left', 'oii-bb-addons'),
                                'center'    =>  __('Center', 'oii-bb-addons'),
                                'right'     =>  __('Right', 'oii-bb-addons')
                            ),
                            'preview'         => array(
                                'type'            => 'css',
                                'selector'        => '.fl-heading',
                                'property'        => 'text-align'
                            )
                        ),
                        'tag'           => array(
                            'type'          => 'select',
                            'label'         => __( 'HTML Tag', 'oii-bb-addons' ),
                            'default'       => 'h2',
                            'options'       => array(
                                'h1'            =>  'h1',
                                'h2'            =>  'h2',
                                'h3'            =>  'h3',
                                'h4'            =>  'h4',
                                'h5'            =>  'h5',
                                'h6'            =>  'h6'
                            )
                        ),
                        'font'          => array(
                            'type'          => 'font',
                            'default'       => array(
                                'family'        => 'Default',
                                'weight'        => 300
                            ),
                            'label'         => __('Font', 'oii-bb-addons'),
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.fl-heading-text'
                            )                   
                        ),
                        'font_size'     => array(
                            'type'          => 'select',
                            'label'         => __('Font Size', 'oii-bb-addons'),
                            'default'       => 'default',
                            'options'       => array(
                                'default'       =>  __('Default', 'oii-bb-addons'),
                                'custom'        =>  __('Custom', 'oii-bb-addons')
                            ),
                            'toggle'        => array(
                                'custom'        => array(
                                    'fields'        => array('custom_font_size')
                                )
                            )
                        ),
                        'custom_font_size' => array(
                            'type'          => 'text',
                            'label'         => __('Custom Font Size', 'oii-bb-addons'),
                            'default'       => '24',
                            'maxlength'     => '3',
                            'size'          => '4',
                            'description'   => 'px'
                        ),
                        'line_height'     => array(
                            'type'          => 'select',
                            'label'         => __('Line Height', 'oii-bb-addons'),
                            'default'       => 'default',
                            'options'       => array(
                                'default'       =>  __('Default', 'oii-bb-addons'),
                                'custom'        =>  __('Custom', 'oii-bb-addons')
                            ),
                            'toggle'        => array(
                                'custom'        => array(
                                    'fields'        => array('custom_line_height')
                                )
                            )
                        ),
                        'custom_line_height' => array(
                            'type'          => 'text',
                            'label'         => __('Custom Line Height', 'oii-bb-addons'),
                            'default'       => '1.4',
                            'maxlength'     => '4',
                            'size'          => '4',
                            'description'   => 'em'
                        ),
                        'letter_spacing'     => array(
                            'type'          => 'select',
                            'label'         => __('Letter Spacing', 'oii-bb-addons'),
                            'default'       => 'default',
                            'options'       => array(
                                'default'       =>  __('Default', 'oii-bb-addons'),
                                'custom'        =>  __('Custom', 'oii-bb-addons')
                            ),
                            'toggle'        => array(
                                'custom'        => array(
                                    'fields'        => array('custom_letter_spacing')
                                )
                            )
                        ),
                        'custom_letter_spacing' => array(
                            'type'          => 'text',
                            'label'         => __('Custom Letter Spacing', 'oii-bb-addons'),
                            'default'       => '0',
                            'maxlength'     => '3',
                            'size'          => '4',
                            'description'   => 'px'
                        )
                    )
                ),
                'r_structure'   => array(
                    'title'         => __( 'Mobile Structure', 'oii-bb-addons' ),
                    'fields'        => array(
                        'r_alignment'   => array(
                            'type'          => 'select',
                            'label'         => __('Alignment', 'oii-bb-addons'),
                            'default'       => 'default',
                            'options'       => array(
                                'default'       =>  __('Default', 'oii-bb-addons'),
                                'custom'        =>  __('Custom', 'oii-bb-addons')
                            ),
                            'toggle'        => array(
                                'custom'        => array(
                                    'fields'        => array('r_custom_alignment')
                                )
                            ),
                            'preview'         => array(
                                'type'            => 'none'
                            )
                        ),
                        'r_custom_alignment' => array(
                            'type'          => 'select',
                            'label'         => __('Custom Alignment', 'oii-bb-addons'),
                            'default'       => 'center',
                            'options'       => array(
                                'left'      =>  __('Left', 'oii-bb-addons'),
                                'center'    =>  __('Center', 'oii-bb-addons'),
                                'right'     =>  __('Right', 'oii-bb-addons')
                            ),
                            'preview'         => array(
                                'type'            => 'none'
                            )
                        ),
                        'r_font_size'   => array(
                            'type'          => 'select',
                            'label'         => __('Font Size', 'oii-bb-addons'),
                            'default'       => 'default',
                            'options'       => array(
                                'default'       =>  __('Default', 'oii-bb-addons'),
                                'custom'        =>  __('Custom', 'oii-bb-addons')
                            ),
                            'toggle'        => array(
                                'custom'        => array(
                                    'fields'        => array('r_custom_font_size')
                                )
                            )
                        ),
                        'r_custom_font_size' => array(
                            'type'          => 'text',
                            'label'         => __('Custom Font Size', 'oii-bb-addons'),
                            'default'       => '24',
                            'maxlength'     => '3',
                            'size'          => '4',
                            'description'   => 'px'
                        ),
                        'r_line_height'     => array(
                            'type'          => 'select',
                            'label'         => __('Line Height', 'oii-bb-addons'),
                            'default'       => 'default',
                            'options'       => array(
                                'default'       =>  __('Default', 'oii-bb-addons'),
                                'custom'        =>  __('Custom', 'oii-bb-addons')
                            ),
                            'toggle'        => array(
                                'custom'        => array(
                                    'fields'        => array('r_custom_line_height')
                                )
                            )
                        ),
                        'r_custom_line_height' => array(
                            'type'          => 'text',
                            'label'         => __('Custom Line Height', 'oii-bb-addons'),
                            'default'       => '1.4',
                            'maxlength'     => '4',
                            'size'          => '4'
                        ),
                        'r_letter_spacing'     => array(
                            'type'          => 'select',
                            'label'         => __('Letter Spacing', 'oii-bb-addons'),
                            'default'       => 'default',
                            'options'       => array(
                                'default'       =>  __('Default', 'oii-bb-addons'),
                                'custom'        =>  __('Custom', 'oii-bb-addons')
                            ),
                            'toggle'        => array(
                                'custom'        => array(
                                    'fields'        => array('r_custom_letter_spacing')
                                )
                            )
                        ),
                        'r_custom_letter_spacing' => array(
                            'type'          => 'text',
                            'label'         => __('Custom Letter Spacing', 'oii-bb-addons'),
                            'default'       => '0',
                            'maxlength'     => '3',
                            'size'          => '4',
                            'description'   => 'px'
                        )
                    )
                )
            )));


/**
 * Register the module and its form settings.
 */
FLBuilder::register_module( 'OiiHeadingModule', $general_tab);