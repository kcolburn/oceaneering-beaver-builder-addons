<?php
/**
 * Plugin Name: Oceaneering Addons for Beaver Builder
 * Plugin URI: http://www.oceaneering.com
 * Description: Custom modules and templates for the Beaver Builder Plugin.
 * Version: 1.0.0
 * Author: Joey Zeringue
 * Author URI: http://www.oceaneering.com
 * Text Domain: oii-bb-addons
 */



// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
final class Oceaneering_BB_Addons {
	/**
     * Holds the class object.
     *
     * @since 1.0.0
     * @var object
     */
    public static $instance;

    /**
	 * Primary class constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct()
	{
		$this->define_constants();

		/* Hooks */
		$this->init_hooks();

		/* Classes */
		//require_once 'classes/class-admin-settings.php';

		/* Includes */
		//require_once 'includes/helper-functions.php';
	}

	/**
	 * Define Oceaneering BB Addons constants.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	private function define_constants()
	{
		define( 'OII_BB_ADDONS_DIR', plugin_dir_path( __FILE__ ) );
		define( 'OII_BB_ADDONS_URL', plugins_url( '/', __FILE__ ) );
		define( 'OII_BB_ADDONS_PATH', plugin_basename( __FILE__ ) );
		define( 'OII_BB_ADDONS_CAT', __('Oceaneering Modules', 'oii-bb-addons') );
	}

	/**
	 * Initializes actions and filters.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function init_hooks()
	{
		add_action( 'init', array( $this, 'load_modules' ) );
		add_action( 'init', array( $this, 'enqueue_assets' ) );
		/*add_action( 'plugins_loaded', array( $this, 'load_templates' ) );*/
		
	}

	/**
	 * Include modules.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function load_modules()
	{
		if ( class_exists( 'FLBuilder' ) ) {

			/* Modules */
			require_once 'modules/oii-heading/oii-heading.php';
        	require_once 'modules/oii-text-image-swap/oii-text-image-swap.php';
        	require_once 'modules/oii-accordion/oii-accordion.php';

			
		}
	}

	/**
	 * Load Templates.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function load_templates() {
		/**
		 * Return if the builder isn't installed or if the current
		 * version doesn't support registering templates.
		 */
		if ( ! class_exists( 'FLBuilder' ) || ! method_exists( 'FLBuilder', 'register_templates' ) ) {
			return;
		}
		
		/**
		 * Register the path to templates.dat file.
		 */
		FLBuilder::register_templates( OII_BB_ADDONS_DIR . 'data/templates.dat' );
	}

	/**
	 * Hide Beaver Builder Core Layouts and Modules if user isn't administrator
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function enqueue_assets() {
		/**
		 * Hide Beaver Builder Core Layouts and Modules if user isn't administrator
		 */
		if (!current_user_can('administrator')) {
			wp_enqueue_script( 'hide-bb', OII_BB_ADDONS_URL . 'assets/js/hide-bb.js', array(), '', true );
			wp_enqueue_style( 'hide-bb', OII_BB_ADDONS_URL . 'assets/css/hide-bb.css' );
		}

		wp_enqueue_style( 'oii-icons', OII_BB_ADDONS_URL . 'assets/css/oii-icons/style.css' );

	}

	/**
	 * Returns the singleton instance of the class.
	 *
	 * @since 1.0.0
	 *
	 * @return object The Oceaneering_BB_Addons object.
	 */
	public static function get_instance()
	{
		if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Oceaneering_BB_Addons ) ) {
			self::$instance = new Oceaneering_BB_Addons();
		}

		return self::$instance;
	}

}
// Load the Oceaneering BB Addons class.
$oii_bb_addons = Oceaneering_BB_Addons::get_instance();